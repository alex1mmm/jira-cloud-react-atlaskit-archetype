package it.pkg;

import com.atlassian.connect.spring.IgnoreJwt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class HelloWorldController {

        @GetMapping("/helloworld")
        //@IgnoreJwt
        public String helloworld() {
            return "helloworld";
        }
}
